  ##BranchStart 0
obrtrol_missiontree_1 = {
	slot = 1
	generic = no
	ai = yes
	potential = {
		OR = {
			tag = Z17
			tag = Z74
		}
		
	}
 	has_country_shield = yes
	obrtrol_trolls_of_dalr = {
		icon = mission_burning_castle
		required_missions = {
			obrtrol_finish_the_small_bear
		}
		position = 3
		provinces_to_highlight = {
			OR = { 	
				region = dalr_region
			}
			NOT = {
				owned_by = ROOT
				is_core = ROOT
			}
		}
		###PROVINCESHIGHLIGHTCODE
		trigger = {
			fegras_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			kaldrland_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			calc_true_if = {
    			amount = 15
    			dalr_region = {
        			type = all
        			country_or_non_sovereign_subject_holds = ROOT
    			}
			}
		}
		effect = {
			add_prestige = 20
			olavslund_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { is_permanent_claim = ROOT }
				}
				add_permanent_claim = ROOT
			}
			nyrford_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { is_permanent_claim = ROOT }
				}
				add_permanent_claim = ROOT
			}
			998 = {
				change_culture = fjord_troll
				change_religion = ROOT
				add_devastation = 20
			}
			969 = {
				change_culture = fjord_troll
				change_religion = ROOT
				add_devastation = 20
			}
			963 = {
				change_culture = fjord_troll
				change_religion = ROOT
				add_devastation = 20
			}
			958 = {
				change_culture = fjord_troll
				change_religion = ROOT
				add_devastation = 20
			}
			medium_decrease_of_human_tolerance_effect = yes				
		}
	}
	obrtrol_big_books_big_runes = {
		icon = mission_consulate_of_the_sea
		required_missions = {
			obrtrol_trolls_of_dalr
		}
		position = 4
		provinces_to_highlight = {
			OR = { 	
				area = elkaesals_slumber_area
				province_id = 976
			}
			NOT = {
				owned_by = ROOT
				is_core = ROOT
			}
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
				owns_core_province = 972
				owns_core_province = 973
				owns_core_province = 974
				owns_core_province = 976
			}
		effect = {
			giants_manse_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { is_permanent_claim = ROOT }
				}
				add_permanent_claim = ROOT
			}
			add_country_modifier = {
				name = "obrtrol_runic_knowledge"
				duration = 10950 #30 years
			}
		}
	}
	obrtrol_take_olavslund = {
		icon = mission_build_up_to_force_limit
		required_missions = {
			obrtrol_big_books_big_runes
		}
		position = 6
		provinces_to_highlight = {
			OR = {
				area = olavslund_area
			}
			###PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			olavslund_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			#MISSIONTRIGGER
		}
		effect = {
			define_advisor = {
				type = inquisitor
				skill = 1
				culture = fjord_troll
				religion = mountain_watchers
				female = yes
				discount = yes
			}
			add_prestige = 20
			naugsvol_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { is_permanent_claim = ROOT }
					}
				add_permanent_claim = ROOT
				}
			esfjall_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { is_permanent_claim = ROOT }
				}
				add_permanent_claim = ROOT
			}
			#MISSIONEFFECT
		}
	}
	obrtrol_brave_the_waters = {
		icon = mission_protect_white_sea_trade
		required_missions = {
			obrtrol_take_olavslund
		}
		position = 7
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			num_of_galley = 12
			}
		effect = {
			add_country_modifier = {
				name = "obrtrol_boats_covered_in_oil"
				duration = 5475 #15 years
			#MISSIONEFFECT
			}
			mawriver_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { is_permanent_claim = ROOT }
				}
				add_permanent_claim = ROOT
			}
			serpentshead_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { is_permanent_claim = ROOT }
				}
				add_permanent_claim = ROOT
			}
			coddorran_heights_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { is_permanent_claim = ROOT }
				}
				add_permanent_claim = ROOT
			}
			storm_isles_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { is_permanent_claim = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	obrtrol_trolls_rule_the_waves = {
			icon = mission_rb_a_mighty_fleet
			required_missions = {
				obrtrol_brave_the_waters
			}
			position = 8
			provinces_to_highlight = {
				##PROVINCESHIGHLIGHTCODE
			}
			trigger = {
				num_of_owned_provinces_with = {
					value = 20
					OR = {
						has_shipyard_building_trigger = yes
						has_dock_building_trigger = yes
					}
				}
			}
			effect = {
				add_country_modifier = {
					name = "obrtrol_naval_expansion"
					duration = 10950 #30 years
			#MISSIONEFFECT
			}
		}
	}
}
 ##BranchEnd 1
 ##BranchStart 2
obrtrol_missiontree_2 = {
	slot = 2
	generic = no
	ai = yes
	potential = {
		OR = {
			tag = Z17
			tag = Z74
		}
	}
	has_country_shield = yes
	obrtrol_draell_labour = {
		icon = mission_smith
		required_missions = {
		}
		position = 3

		provinces_to_highlight = {
			OR = {
				culture = west_dalr
				culture = east_dalr
				culture = olavish
			##PROVINCESHIGHLIGHTCODE
			}
		NOT = { country_or_non_sovereign_subject_holds = ROOT }
		}
		trigger = {
			treasury = 100
			adm_tech = 3
			num_of_owned_provinces_with = {
				value = 8
				culture_group = gerudian
			}
		}
		effect = {
		large_decrease_of_human_tolerance_effect = yes
			capital_scope = {
				add_province_modifier = {
					name = obrtrol_draell_labour
					duration = 7300 #20 years
				}
				add_province_modifier = {
					name = obrtrol_expanded_infrastructure
					duration = -1
				}
				add_building_construction = {
					building = mage_tower
					speed = 0.5
					cost = 0.2
				}
			}
		}
	}
	obrtrol_urviksten_giants_forge = {
		icon = mission_smith
		required_missions = {
			obrtrol_big_books_big_runes
		}
		position = 5
		provinces_to_highlight = {
			OR = {
				province_id = 984
			}
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			owns_core_province = 984
				984 = { base_production = 8
			#MISSIONTRIGGER
			}
		}
		effect = {
			984 = {
				add_permanent_province_modifier = {
					name = "obrtrol_giant_forge"
					duration = -1
				}
			}
		}
	}
	obrtrol_giants_home_reclaimed = {
		icon = mission_rein_in_the_velamas
		required_missions = {
			obrtrol_take_olavslund
		}
		position = 7
		provinces_to_highlight = {
			province_id = 985
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			owns_core_province = 985
		}
		effect = {
			add_prestige = 30
			drowned_giant_isles_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { is_permanent_claim = ROOT }
				}
				add_permanent_claim = ROOT
			}
			add_country_modifier = {
				name = "obrtrol_northen_reclamation"
				duration = 18250 #50 years
			}
		}
	}
}



 ##BranchEnd 2
 ##BranchStart 3
obrtrol_missiontree_3 = {
	slot = 3
	generic = no
	ai = yes
	potential = {
		OR = {
			tag = Z17
			tag = Z74
		}
		
	}
	has_country_shield = yes
	
  ##Mission start
 	obrtrol_our_last_chance = {
 		icon = mission_build_up_to_force_limit
 		required_missions = {
			
 		}
 		position = 1
 		provinces_to_highlight = {
 			##PROVINCESHIGHLIGHTCODE
 		}
 		trigger = {
 			OR {
 				army_size_percentage = 1
 				defensive_war_with = Z08
 			}
 		}
 		effect = {
 		add_country_modifier = {
 				name = "obrtrol_drums_of_war"
 				duration = 5475 #15 years
 				}
 				define_general = {
 					name = "Fulgurk"
 					fire = 1
 					shock = 5
 					manuever = 2
 					siege = 2
 					}
 				ismark_area = {
 					limit = {
 						NOT = { is_core = ROOT }
 						NOT = { is_permanent_claim = ROOT }
 					}
 					add_permanent_claim = ROOT
 				}
 				kaldrland_area = {
 					limit = {
 						NOT = { is_core = ROOT }
 						NOT = { is_permanent_claim = ROOT }
 					}
 					add_permanent_claim = ROOT
 				}
 				sidaett_area = {
 					limit = {
 						NOT = { is_core = ROOT }
 						NOT = { is_permanent_claim = ROOT }
 					}
 					add_permanent_claim = ROOT
 				}
 		}
 	}
 	##Mission End
 	##Mission Start
 	obrtrol_finish_the_small_bear = {
 		icon = mission_manchu_warrior
 		required_missions = {
 			obrtrol_our_last_chance
 		}
 		position = 2
 		provinces_to_highlight = {
 			OR = {
 		area = kaldrland_area
 		area = sidaett_area
 		area = ismark_area ##PROVINCESHIGHLIGHTCODE
 			}
 		}
 		trigger = {
 			kaldrland_area = {
 				type = all
 				country_or_non_sovereign_subject_holds = ROOT
 			}
 			sidaett_area = {
 				type = all
 				country_or_non_sovereign_subject_holds = ROOT
 			}
 			ismark_area = {
 				type = all
 				country_or_non_sovereign_subject_holds = ROOT
 			}			##MISSIONTRIGGER
 		}
 		effect = {
 		add_prestige = 20
 		elkaesals_slumber_area = {
 				limit = {
 					NOT = { is_core = ROOT }
 					NOT = { is_permanent_claim = ROOT }
 					}
 				add_permanent_claim = ROOT
 			}
 		dalr_region = {
 				limit = {
 					NOT = { is_core = ROOT }
 					NOT = { is_permanent_claim = ROOT }
 				}
 				add_permanent_claim = ROOT	#MISSIONEFFECT
 			}
 		}
 	}	
 	##Mission End
 	##Mission Start
 	obrtrol_organize_the_tribes = {
 		icon = mission_monarch_in_throne_room
 		required_missions = {
 			obrtrol_finish_the_small_bear
 		}
 		position = 3
 		provinces_to_highlight = {
 			##PROVINCESHIGHLIGHTCODE
 		}
 		trigger = {

 			dip_power = 100

		}
			effect = {
				add_dip_power = -100
				change_government_reform_progress = 15
				add_country_modifier = { 
				name = "obrtrol_centralized_advice"
				duration = 9125 #25 years 
				#MISSIONEFFECT
			}
		}
	}
 ##Mission End
 ##Mission start
	obrtrol_restore_the_capital = {
		icon = mission_colonial
		required_missions = {
			obrtrol_organize_the_tribes obrtrol_draell_labour
		}
		position = 4
		provinces_to_highlight = {
		province_id = 995
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			owns_core_province = 995
			995 = {
				development = 20
				has_tax_building_trigger = yes
			##MISSIONTRIGGER
			}
		}
		effect = {
			add_country_modifier = { 
				name = "obrtrol_mountain_runes_amplified"
				duration = -1
			}
		995 = {
					add_base_tax = 2 }
			#MISSIONEFFECT
		}
	}
 ##Mission End
 ##Mission start
	obrtrol_secure_our_past = {
		icon = invest_in_the_rich_trades
		required_missions = {
			obrtrol_restore_the_capital
		}
		position = 6
		provinces_to_highlight = {
		province_id = 985
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			985 = {
				has_tax_building_trigger = yes
				has_building = mage_tower
			}
			985 = {
				country_or_non_sovereign_subject_holds = ROOT
			}
			##MISSIONTRIGGER
		}
		effect = {
			985 = {
				change_culture = ROOT
				change_religion = ROOT
				add_base_tax = 2
				add_base_production = 1
				add_base_manpower = 1
 #MISSIONEFFECT
			}
		}
	}
 ##Mission End
 ##Mission start
	obrtrol_prepare_the_link = {
		icon = mission_white_tower
		required_missions = {
			obrtrol_secure_our_past
		}
		position = 7
		provinces_to_highlight = {
			province_id = 995
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			995 = {
				owned_by = ROOT
				development = 25
				has_production_building_trigger = yes
				has_tax_building_trigger = yes
				has_building = mage_tower
			}
		}
		effect = {
			add_adm_power = 100
			add_dip_power = 100
			add_mil_power = 100
			985 = {
				add_permanent_province_modifier = {
					name = "obrtrol_towers_reclaimed"
					duration = -1
				}
			}
			995 = {
				add_permanent_province_modifier = {
					name = "obrtrol_runic_ward"
					duration = -1
				}
			}
		}
	}
 ##Mission End
 ##Mission start
	obrtrol_geruds_legacy_seized = {
		icon = mission_empire
		required_missions = {
			obrtrol_giants_home_reclaimed obrtrol_prepare_the_link obrtrol_shamanic_reunion
		}
		position = 8
		provinces_to_highlight = {
			province_id = 995
			province_id = 985
		}
		trigger = {
			is_at_war = no
			995 = {
				country_or_non_sovereign_subject_holds = ROOT
			}
			985 = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			if = {
				limit = { exists = Z74 }
				add_treasury = 100
			}
			else = {
				gerudia_superregion = {
					limit = {
						NOT = { is_core = ROOT }
						NOT = { is_permanent_claim = ROOT }
					}
				add_permanent_claim = ROOT
				}
				hidden_effect = { restore_country_name = yes }
				change_tag = Z74
				swap_non_generic_missions = yes
				add_prestige = 25
				set_government_rank = 2
				change_government = monarchy
				add_government_reform = northen_custodians
				if = {
					limit = { has_custom_ideas = no }
					country_event = { id = ideagroups.1 } #Swap Ideas
				}
			}
		}
	}
}
 ##BranchEnd 3
 ##BranchStart 4
obrtrol_missiontree_4 = {
	slot = 4
	generic = no
	ai = yes
	potential = {
		OR = {
			tag = Z17
			tag = Z74
		}
		
	}
	has_country_shield = yes
	obrtrol_find_forest_troll = {
 		icon = mission_feast
 		required_missions = {
 			obrtrol_organize_the_tribes
 		}
 		position = 4
 		provinces_to_highlight = {
 			##PROVINCESHIGHLIGHTCODE
 		}
 		trigger = {
 			dip_power = 50 
 		}
 		effect = {
 			add_dip_power = -50
 			add_country_modifier = {
 				name = "obrtrol_colonial_enthusiasm"
 				duration = 54750 #150 years
 			}
 		}
 	}
  ##Mission start
 	obrtrol_contact_forest_trolls = {
 		icon = mission_alone_in_the_wood
 		required_missions = {
 			obrtrol_find_forest_troll
 		}
 		position = 5
 		provinces_to_highlight = {
 			area = gullmork_area
 		}
 		trigger = {
 			dip_power = 50
			gullmork_area = {
 				country_or_non_sovereign_subject_holds = ROOT
 			}
 		}
 		effect = {
 			add_dip_power = -50
 				country_event = { 
					id = flavor_obrtrol.1
 			}
 			#MISSIONEFFECT
 		}
 	}
 	##Mission End
 	##Mission Start
 	obrtrol_shamanic_reunion = {
 		icon = mission_build_up_to_force_limit
 		required_missions = {
 			obrtrol_contact_forest_trolls
 		}
 		position = 7
 		provinces_to_highlight = {
 			##PROVINCESHIGHLIGHTCODE
 		}
 		trigger = {
 			employed_advisor = {
 				category = ADM
 				is_male = no
 			}
 			employed_advisor = {
 				category = DIP
 				is_male = no
 			}
 			employed_advisor = {
 				category = MIL
 				is_male = no
 			}
 			##MISSIONTRIGGER
 		}
 		effect = {
 		 	define_conquistador = {
 				name = "Gymir"
 				fire = 1
 				shock = 3
 				manuever = 5
 				siege = 0
 				}
 			add_government_reform = troll_forebearer_teachings
 			#MISSIONEFFECT
 		}
 	}
 	##Mission End
}
  ##BranchEnd 4
  ##BranchStart 5
obrtrol_missiontree_5 = {
 	slot = 5
 	generic = no
 	ai = yes
	potential = {
		OR = {
			tag = Z17
			tag = Z74
		}
		
	}
 	has_country_shield = yes

	obrtrol_elf_and_onion_soup = {
 		icon = mission_burning_separation
 		required_missions = {
 		}
 		position = 2
 			##PROVINCESHIGHLIGHTCODE
 		trigger = {
			Z74 = {
				any_owned_province = {
					culture_group = elven
				}
			}
 		}
 		effect = {
			add_country_modifier = {
			name = "obrtrol_yummy_soup"
			duration = 7300 #20 years
 			#MISSIONEFFECT
			}
		}
	}
  ##Mission start
 	obrtrol_expand_the_mines = {
 		icon = mission_burning_separation
 		required_missions = {
 			obrtrol_finish_the_small_bear
 		}
 		position = 3
 		provinces_to_highlight = {
 			OR = {
 			province_id = 1000
 			province_id = 994
 			}
 			##PROVINCESHIGHLIGHTCODE
 		}
 		trigger = {
 			1000 = {
 				owned_by = ROOT
 				base_production = 5
 			}
 			994 = {
 				owned_by = ROOT
 				base_production = 5
 			}		##MISSIONTRIGGER
 		}
 		effect = {
 				if = {
 					limit = { 994 = { NOT = { trade_goods = iron } } }
 					994 = { change_trade_goods = iron
 					add_base_manpower = 2 }
 					limit = { 1000 = { NOT = { trade_goods = copper } } }
 					1000 = { change_trade_goods = copper
 					add_base_manpower = 2 }
 				994 = { add_human_minority_size_effect = yes
 					add_province_modifier = {
						name = "obrtrol_slave_mine"
						duration = -1
					}
				}
 				1000 = { add_human_minority_size_effect = yes
				add_province_modifier = {
					name = "obrtrol_slave_mine"
					duration = -1
					}
				}
 			}
 		medium_decrease_of_human_tolerance_effect = yes
 			#MISSIONEFFECT
 		}
 	}
 	##Mission End
 	##Mission Start
 	obrtrol_mammoths_in_dalr = {
 		icon = mission_indian_soldier_elephant
 		required_missions = {
 			obrtrol_expand_the_mines
 		}
 		position = 4
 		provinces_to_highlight = {
 			OR = {
 			province_id = 955 
 			province_id = 997 
 			}##PROVINCESHIGHLIGHTCODE
 		}
 		trigger = {
 			955 = {
 				owned_by = ROOT
 				base_production = 7
 			}
 			997 = {
 				owned_by = ROOT
 				base_production = 7
 			}			##MISSIONTRIGGER
 		}
 		effect = {
 			955 = {
				add_province_modifier = {
					name = "obrtrol_mammoth_grazing"
					duration = -1
				}
				change_culture = fjord_troll
				change_religion = ROOT
 				add_base_production = 2 
 			}
 			997 = {				
 				add_province_modifier = {
					name = "obrtrol_mammoth_grazing"
					duration = -1
				}
				change_culture = fjord_troll
				change_religion = ROOT
 				add_base_production = 2 
 			}
		small_decrease_of_human_tolerance_effect = yes
 		}
 	}
 	##Mission End
 	##Mission Start
 	obrtrol_build_the_zein_market = {
 		icon = mission_merchant_trip
 		required_missions = {
 			obrtrol_mammoths_in_dalr
 		}
 		position = 5
 		provinces_to_highlight = {
 			province_id = 959
 			##PROVINCESHIGHLIGHTCODE
 		}
 		trigger = {
			treasury = 400
 			959 = {
 				province_has_center_of_trade_of_level = 2
 				has_production_building_trigger = yes
 				has_trade_building_trigger = yes
 			}
 		}
 		effect = {
			add_treasury = -400
 			959 = {
 			 	if = {
 					limit = { 959 = { NOT = { trade_goods = ivory } } }
 					change_trade_goods = ivory 
 			}
 			change_province_name = "Zeinazmark"
 			add_center_of_trade_level = 1
			change_culture = fjord_troll
			change_religion = ROOT
 			add_base_production = 2 
 			add_province_modifier = {
				name = "obrtrol_bone_market"
				duration = -1
				}
 			}
 		}
 	}
 	##Mission End
 	##Mission Start
 	obrtrol_gold_in_far_gerudia = {
 		icon = mission_entrepot_of_india
 		required_missions = {
 			obrtrol_build_the_zein_market
 		}
 		position = 6
 		provinces_to_highlight = {
 			province_id = 2831
 			##PROVINCESHIGHLIGHTCODE
 		}
 		trigger = {
 			2831 = {
 				owned_by = ROOT
 				base_production = 7
 			} #MISSIONTRIGGER
 		}
 		effect = {
 			if = {
 				limit = { 2831 = { NOT = { trade_goods = gold } } }
 				2831 = { change_trade_goods = gold
 				add_base_manpower = 2 
 				}
 			} #MISSIONEFFECT
 		}
 	}
  	obrtrol_wet_fur_coast = {
 		icon = mission_build_up_to_force_limit
 		required_missions = {
 			obrtrol_gold_in_far_gerudia
 		}
 		position = 7
		provinces_to_highlight = {
			OR = {
				province_id = 977
				province_id = 982
				province_id = 980
			}
 		}
 		trigger = {
 		 	977 = {
 				owned_by = ROOT
 				base_production = 5
 			}
 		 	982 = {
 				owned_by = ROOT
 				base_production = 5
 			}
 		 	980 = {
 				owned_by = ROOT
 				base_production = 5
 			}
 		}
 		effect = { 			
			977 = {
				if = {
				limit = { NOT = { trade_goods = wool } }
					change_trade_goods = wool
				}
				change_culture = fjord_troll
				change_religion = ROOT
				add_base_manpower = 2 
				add_province_modifier = {
				name = "obrtrol_mammoth_grazing"
					duration = -1
				}
			}
			982 = {
				if = {
				limit = { NOT = { trade_goods = wool } }
					change_trade_goods = wool
				}
				change_culture = fjord_troll
				change_religion = ROOT
				add_base_manpower = 2 
				add_province_modifier = {
				name = "obrtrol_mammoth_grazing"
					duration = -1
				}
			}
 			980 = {
 				change_culture = fjord_troll
				change_religion = ROOT
 				add_base_manpower = 2 		
 				add_province_modifier = {
					name = "obrtrol_mammoth_grazing"
					duration = -1
				}
 			}
		small_decrease_of_human_tolerance_effect = yes
		}
	}
 	##Mission End
 	##Mission Start
 	obrtrol_home_is_where_the_heart_is = {
 		icon = mission_green_village
 		required_missions = {
 			obrtrol_shamanic_reunion
 		}
 		position = 8
 		provinces_to_highlight = {
 		province_id = 6173
 			##PROVINCESHIGHLIGHTCODE
 		}
 		trigger = {
 		adm_power = 100
 			6173 = {
 				owned_by = ROOT
 				base_manpower = 5
 			} #MISSIONTRIGGER
 		}
 		effect = {
 		add_adm_power = -100
 			if = {
 				limit = { 6173 = { NOT = { trade_goods = ivory } } }
 				limit = { 6173 = { NOT = { trade_goods = gold } } }
 				limit = { 6173 = { NOT = { trade_goods = gems } } }
 				limit = { 6173 = { NOT = { trade_goods = mithril } } }
 				6173 = { 
 					change_trade_goods = ivory
 					add_base_production = 1
 					add_base_manpower = 2 
 				}
 			}
 			
 			add_province_modifier = {
				name = "obrtrol_mammoth_grazing"
				duration = -1
			}
 		}
 	}
}

 ##BranchEnd 5

